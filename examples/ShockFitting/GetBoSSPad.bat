dotnet publish ../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./1DSpcTmAdvection/binaries
dotnet publish ../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./1DSpcTmBrgs/AccShock/binaries
dotnet publish ../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./1DSpcTmBrgs/StraightShock/binaries
dotnet publish ../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./1DSpcTmEuler/ShockAcousticInteraction/binaries
dotnet publish ../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./2DSteadyEuler/BowShock/binaries
dotnet publish ../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./2DSteadyEuler/WedgeFlow/binaries