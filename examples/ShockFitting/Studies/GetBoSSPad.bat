dotnet publish ../../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./ConvergenceStudy
dotnet publish ../../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./ComparisonShockCapturing
dotnet publish ../../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./ComparisonOptimizationProblem
dotnet publish ../../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./ComparisonLevelSets
dotnet publish ../../../src/L4-application/BoSSSpad/BoSSSpad.csproj -c Release -v q -o ./ComparisonFluxes